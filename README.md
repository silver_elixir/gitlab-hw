# Gitlab Hw

Task 1:
- [ ] 1. Connect your own runner to Gitlab.
- [ ] 2. Create GitlabCI for python+redis app (homework-3.7z -Week 3. Tasks).
- [ ] 3. Use GitLab registry to store images.
- [ ] 4. Rollout a container from the new image on a server (not a runner).
- [ ] 5. As an answer - link to gitlab in Google doc

